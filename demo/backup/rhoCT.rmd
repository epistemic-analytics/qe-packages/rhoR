---
  output: html_document
---
 

``{r, echo=FALSE}
run.bench <- function(oK, fsb, hsl, fsl = 10000, times = 10) {
  microbenchmark::microbenchmark(
    rho.set = calcRho(observedKappa = oK, fullSetBaserate = fsb, handSetLength = hsl, fullSetLength = fsl),
    rho.ct = calcRho2(observedKappa = oK, fullSetBaserate = fsb, handSetLength = hsl, fullSetLength = fsl),
    times = times
  )
}
run.bench(0.8, 0.3, 20, 1000)
``


microbenchmark::microbenchmark(
  getHandSet(set, 20, 0.2),
  getHandCT(ct, 20, 0.2),
  times = 10
)

